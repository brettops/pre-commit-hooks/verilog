# Verilog pre-commit hooks

[![tag](https://img.shields.io/gitlab/v/tag/brettops/hooks/verilog)](https://gitlab.com/brettops/hooks/verilog/-/tags)
[![pipeline status](https://gitlab.com/brettops/hooks/verilog/badges/main/pipeline.svg)](https://gitlab.com/brettops/hooks/verilog/-/commits/main)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)

This project provides [pre-commit](https://pre-commit.com/) hooks to support the development of Verilog
code.

## Usage

Add the desired hooks to your `.pre-commit-config.yaml` file. Note the empty
`rev` tag:

```yaml
repos:
  - repo: https://gitlab.com/brettops/hooks/verilog
    rev: ""
    hooks:
      - id: verible-verilog-format
      - id: verible-verilog-lint
```

Run `pre-commit autoupdate` to update to the latest hook versions:

```bash
pre-commit autoupdate
```

Run `pre-commit` directly or install as a Git pre-commit hook:

```bash
# directly
pre-commit

# as a Git hook
pre-commit install
git commit -m "..."
```

## Hooks

Verible hooks use the [BrettOps verible
container](https://gitlab.com/brettops/containers/verible) to provide the
Verible CLI, so Docker is required to use this pre-commit hook.

### `verible-verilog-format`

Format Verilog code using `verible-verilog-format`.

### `verible-verilog-lint`

Find Verilog code quality issues using `verible-verilog-lint`.
